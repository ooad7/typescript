// function printStatusCode(code: string | number) {
//     console.log(`My status code is ${code}.`)
// }
function printStatusCode(code: string | number) {
    if (typeof code === 'string') {
        console.log(`My status code is ${code.toUpperCase()}.`);
    }else{
        console.log(`My status code is ${code}.`);
    }
}
//Two values can be configured.
printStatusCode(404);
printStatusCode('abc');